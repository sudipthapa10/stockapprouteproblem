
import {Component, OnInit} from '@angular/core';
import {wishList} from './wishList';
import {ownList} from './ownList';

import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {WishListService} from "../../services/wishList.service";


@Component({
    moduleId: module.id,
    selector: 'wish-list-form',
    templateUrl: './wishList.component.html',
    providers : [ WishListService]
    // styleUrls: [ './dashboard.component.css' ]
})

export class WishListComponent implements OnInit{
    wish :wishList[] = [];
    own :ownList[] = [];
    lists : any =[];
    public submitted: boolean = false;

    onSubmit() { this.submitted = true; }
    constructor(private service:WishListService) {}

    ngOnInit(): void {
        this.service.getWishList().subscribe
        (lists => { console.log(lists);this.lists = lists;} );
    }

   addToWishList(event, list):void {
        event.preventDefault();
        //hardcoded userName , this need to be changed with authentication
        var newWishList = {
            _id : "sudipthapa10",
            ind : "wish",
            ticker : list.ticker,
            price : list.price
        };
        var newObj = {
            ticker : list.ticker,
            price : list.price
        }
        this.service.saveStockList(newWishList).subscribe(res => {
            for (var i=0; i<res.length; i++) {
               if (res[i].ticker == newObj["ticker"].toUpperCase() && res[i]["price"] == newObj["price"]) {
                   console.log(this.lists.wishList);
                    this.lists.wishList.push(res[i]);
                }
            }
        });
    }
    addToOwnedList(event, list):void {
        event.preventDefault();
        //hardcoded userName , this need to be changed with authentication
        var newWishList = {
            _id : "sudipthapa10",
            ind : "own",
            ticker : list.ownticker,
            price : list.ownprice
        };
        var newObj = {
            ownticker : list.ownticker,
            ownprice : list.ownprice
        }
        this.service.saveStockList(newWishList).subscribe(res => {
            for (var i=0; i<res.length; i++) {
                console.log(res[i].ticker)
                if (res[i].ticker == newObj["ownticker"].toUpperCase() && res[i].price == newObj["ownprice"]) {
                    this.lists.ownList.push(res[i]);
                }
            }
        });
    }
    deleteWishList(event, _id):void {
        console.log(_id);
        event.preventDefault();
        var obj = {
            childId : _id
        }
        this.service.deleteWishList(obj).subscribe(
            data => {
                var currentList = this.lists.wishList;
                console.log(currentList);
                for (var i=0; i<currentList.length; i++) {
                    if (currentList[i]._id == _id) {
                        this.lists.wishList.splice(i,1);
                        break;
                    }
                }
            }
        );
    }
    deleteOwnList(event, _id):void {
        event.preventDefault();
        var obj = {
            childId : _id
        }
        this.service.deleteOwnList(obj).subscribe(
            data => {
                var currentList = this.lists.ownList;
                console.log(currentList);
                for (var i=0; i<currentList.length; i++) {
                    if (currentList[i]._id == _id) {
                        this.lists.ownList.splice(i,1);
                        break;
                    }
                }
            }
        );
    }
}
