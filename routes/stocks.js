var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db = mongojs('mongodb://prix:prix@ds019058.mlab.com:19058/stock_predict_goru', ['tasks']);

// Get All Tasks
router.get('/stocks', function(req, res, next){
    db.tasks.find(function(err, stocks){
        if(err){
            res.send(err);
        }
        res.json(stocks);
    });
});