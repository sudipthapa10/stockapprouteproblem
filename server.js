var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session      = require('express-session');
var passport = require('passport');
var flash    = require('connect-flash');
var morgan = require('morgan');
var index = require('./routes/index');
var port = process.env.PORT || 3000;

var app = express();

//View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// Set Static Folder
app.use(express.static(path.join(__dirname, 'client')));

app.use(morgan('dev'));
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms

// required for passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session



require('./server/models/db');
var routesApi = require('./server/routes/taskRoute');
app.use('/task', routesApi);
var stockApi = require('./server/routes/stocksRoute');
app.use('/api', stockApi);
var stockListApi = require('./server/routes/userProfileRoute');
app.use('/usr', stockListApi);
//if there is no path specified in api , we will let angular 2 routes handle it and server the home page
app.use('/*', index);

// we also need to handle error for unspecified route in angular 2,
// this line of code is not wotking , we may have to do something with angular 2 routes

app.use(function(err,req,res,next) {
    res.writeHead(err.status || 500, {
        'WWW-Authenticate': 'Basic',
        'Content-Type': 'text/plain'
    });
    res.end(err.message);
});



app.listen(port, function(){
    console.log('Server started on port '+port);
});