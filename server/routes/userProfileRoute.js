var express = require('express');
var usrProfile = require ('../controller/userProfileController');

var router= express.Router();

router.get('/stocks',usrProfile.getStocksLists);
router.post('/saveList',usrProfile.saveWishList);
router.post('/deleteWish',usrProfile.deleteWishList);

router.post('/deleteOwn',usrProfile.deleteOwnList);



module.exports = router;