
import {Component, OnInit} from '@angular/core';
import {Stock} from './Stock';
import {StockService} from "../../services/stock.service";


@Component({
    moduleId: module.id,
    selector: 'stock-body',
    templateUrl: './stock.component.html'
    // styleUrls: [ './dashboard.component.css' ]
})

export class StockComponent implements OnInit{
    hotStock :Stock[] = [];
    stock : Stock ;
    constructor(private stockService:StockService) {}

    ngOnInit(): void {
        this.stockService.getStocks().subscribe
        (hotStock => this.hotStock = hotStock);

    }
    addToOwnedList(event):void {
        event.preventDefault();

    }

}
