
import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {StockComponent} from './components/stock/stock.component';
import {WishListComponent} from './components/wishList/wishList.component';
import {AboutComponent} from './components/about/about.component';

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home',  component: StockComponent },
    { path: 'about',  component: AboutComponent }
 /*   { path: 'dashboard',  component: DashboardComponent },
    { path: 'detail/:id', component: HeroDetailComponent },
    { path: 'heroes',     component: HeroesComponent }*/
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}

