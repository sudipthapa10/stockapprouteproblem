import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
//import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { wishList } from '../components/wishList/wishList';


@Injectable()
export class WishListService {
    private headers = new Headers({'Content-Type': 'application/json'});
    constructor(private http:Http){
        console.log('wish List Service Initialized...');
    }

    getWishList(){
        var response =  this.http.get('/usr/stocks')
            .map(response => response.json());
        return response;
    }

   saveStockList(lists){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/usr/saveList', JSON.stringify(lists), {headers: headers})
            .map(res => res.json());
    }

    deleteWishList(id) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/usr/deleteWish', JSON.stringify(id), {headers: headers})
            .map(res => res.json());
    }

    deleteOwnList(id) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/usr/deleteOwn', JSON.stringify(id), {headers: headers})
            .map(res => res.json());
    }

}