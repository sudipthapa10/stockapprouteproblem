module.exports = {
    init: function (schedule, stockCtrl ) {
        var rule = new schedule.RecurrenceRule();
        rule.minute = new schedule.Range(0, 59, 1);
        schedule.scheduleJob(rule, function () {
            //call startSchedule function, using self because of javascript closure
            stockCtrl.startSchedule(false); // parameter false means load to mongoDB not elstic
        });
    }

}








