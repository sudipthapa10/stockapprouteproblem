
var stockModel = require ('../models/stock');
var http = require('https');
var fs = require('fs');
var dateformat = require('dateformat');

module.exports = {
    counter :0,
    getStocks: function (req, res) {
        stockModel.find({}).where('openPrice')
            .exec(function (err, collection) {
                res.send(collection);
            })
    },
    saveData :function (data) {
        data.save( function(err){
            if(err)
                console.log("error happened during writing into database . Error Sucks");
            else {
                module.exports.getStocks();
            }
        });
    },
    startSchedule: function (loadElastic, wstream, dates, count) {
        if (loadElastic) {
        }
        var self = this;
        var url = this.getUrl(dates[count], dates[count+1]);
        console.log(url);

        http.get(url, function (res) {
            var body = '';
            res.on('data', function (chunk) {
                body += chunk;
            });
            res.on('end', function () {

                var fbResponse = JSON.parse(body);
                if (loadElastic) {
                    if (fbResponse.datatable.data != undefined && fbResponse.datatable.data.length>0) {
                        self.processData(fbResponse.datatable.data,true,wstream, count,dates);
                    } else{
                        self.startSchedule(true,wstream,dates, count+1);
                    }


                } else {
                    self.processData(fbResponse.datatable.data,false ,null);
                }

            });
        }).on('error', function (e) {
            console.log("Got an error: ", e);
        });
    },
    getTodayDate : function() {
        var date =  Date.now();
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();
        var  newDate = year.toString() +  + month.toString() +  + day.toString();
        return newDate;
    },
    getTomorrowDate : function() {
        var currentDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
        var day = currentDate.getDate()
        var month = currentDate.getMonth() + 1
        var year = currentDate.getFullYear();
        console.log(year.toString()+ month.toString() + day.toString());

    },
    getUrl : function(start ,end){
        var self = this;
      //  var todayDate = this.getTodayDate();
      //  var tomorrowDate = this.getTomorrowDate();
       var  todayDate = start;
       var  tomorrowDate = end;
        var url = 'https://www.quandl.com/api/v3/datatables/WIKI/PRICES.json?date.gte='+
            todayDate+'&date.lt='+tomorrowDate+'&api_key=n5whJFJt6J8yU5ba4W55';
        return url;
    },
    processData : function(fullStockData ,loadElastic,wstream,count,dates) {
        var stock = null;
        var self = this;
        if (fullStockData == undefined || fullStockData.length < 1) {
            console.log("data is undefined");
            return;
        }

        for(var i=0; i<fullStockData.length; i++){
            var  attr = fullStockData[i];
            var stock ;
            if(loadElastic) {
               stock = module.exports.createSchemaForElastic(attr);
            }
            else {
                stock = this.createSchema(attr);
            }

            if( stock.percentChange >= 5 || stock.percentChange <=-5) {
                if(loadElastic) {
                    module.exports.prepareAndLoadToElastic(JSON.stringify(stock), wstream);


                } else {
                    module.exports.saveData(stock);
                }
                self.counter += 1;
            }
        }
        if (count <dates.length) {
            console.log("Loaded "+ self.counter);
            this.startSchedule(true,wstream,dates, count+1);

        } else {
            wstream.end();
            console.log("Data Loading complete");

        }





    },
    createSchema : function(data) {
        var stock = new stockModel();
        stock.ticker = data[0];
        stock.date = data[1];
        stock.openPrice = data[2];
        stock.closePrice = data[5];
        stock.volume = data[13]
        stock.percentChange = (((data[5] / data[2]) * 100) - 100).toFixed(2);
       // stock.lastChange = new Date();
        return stock;
    },
    createSchemaForElastic :function(data) {
        var stock = {
            "ticker" : data[0],
            "date" : data[1],
            "openPrice" : data[2],
        "closePrice" : data[5],
        "volume" : data[13],
        "percentChange" : Math.round((((data[5] / data[2]) * 100) - 100))
        }
        return stock;

    },
    saveFromRouter : function(req, res, next) {
        var data = req.body;
        var stock = new stockModel();
        stock.ticker = data.ticker;
        stock.date = data.date;
        stock.openPrice = data.openPrice;
        stock.closePrice = data.closePrice;
        stock.percentChange = data.percentChange;
        stock.lastChange = new Date();
        module.exports.saveData(stock);
    },
    writeToElasticFile : function() {
       var dates = module.exports.getDateRange();
       var wstream = fs.createWriteStream('C:/Training/test.json');
        this.loadedCount = 0;
        module.exports.startSchedule(true, wstream ,dates, this.loadedCount);
    },
    prepareAndLoadToElastic : function(data, wstream) {
        wstream.write('{"index":{"_index":"stocks","_type":"metrics"}}\n')
        wstream.write(data+'\n');

    }
    ,getDateRange : function() {
            var now = new Date();
            var daysOfYear = [];
            for (var d = new Date(2015,00,00); d <= now; d.setDate(d.getDate() + 1)) {
                console.log("her");
                daysOfYear.push(dateformat(d,"yyyymmdd"));
            }
            console.log(daysOfYear);
           return daysOfYear;
    }

}

