var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");

var stockSchema = mongoose.Schema({
    ticker:{
        type: String
    },
    date :{
        type: Date
    },
    openPrice :{
        type: Number
    },
    closePrice : {
        type : Number
    },
    percentChange: {
        type : Number
    },
    lastChange : {
        type : Date
    }
});


module.exports = mongoose.model('stocks123', stockSchema);




