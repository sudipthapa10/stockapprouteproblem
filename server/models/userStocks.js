var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");

var listedStockSchema = mongoose.Schema({
    ticker :{
        type : String
    },
    price : {
        type : Number
    },
    dateAdded : {
        type : Date

    }
});

/*var userStockSchema = mongoose.Schema({
    userId:{
        type: String
    },
    dateCreated :{
        type: Date
    },
    ownList :[listedStockSchema],
    wishList : [listedStockSchema]

});*/


var userStockSchema = mongoose.Schema({
    _id : {
        type : String
    },
    userId:{
        type: String
    },
    dateCreated :{
        type: Date
    },
    wishList : [{
        ticker :{
            type : String
        },
        price : {
            type : Number
        },
        dateAdded : {
            type : Date

        }}],
    ownList :[{
        ticker :{
            type : String
        },
        price : {
            type : Number
        },
        dateAdded : {
            type : Date

        }  }]

});



module.exports = mongoose.model('owned_stocks', userStockSchema);




