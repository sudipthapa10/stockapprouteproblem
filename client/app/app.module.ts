import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {NavComponent} from './components/navbar/navbar.component';
import {StockComponent} from './components/stock/stock.component';
import {WishListComponent} from "./components/wishList/wishList.component";
import {AboutComponent} from './components/about/about.component';
import { AppRoutingModule }     from './routing.module';
import {StockService} from "./services/stock.service";


@NgModule({
  imports:      [ BrowserModule, HttpModule, FormsModule ,  AppRoutingModule],
  declarations: [AppComponent ,NavComponent, StockComponent, AboutComponent,WishListComponent] ,
  bootstrap: [AppComponent],
  providers : [StockService]
})
export class AppModule { }
