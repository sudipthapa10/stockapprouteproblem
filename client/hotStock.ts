
export class hotStock{
    "ticker": string;
    "date": string;
    "openPrice": number;
    "closePrice": number;
    "percentChange": number;
    "lastChange": string;
}