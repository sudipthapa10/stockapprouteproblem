var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
LocalStrategy = require('passport-local').Strategy;

var db = mongojs('mongodb://prix:prix@ds019058.mlab.com:19058/stock_predict_goru', ['user_profile']);




router.put('/addStocks', function(req, res, next){
    var list = req.body;
    var myList = {};
    if(!list.userId){
        res.status(400);
        res.json({
            "error":"Bad Data"
        });
    }
    var whatList = "";
    myList.ticker = list.ticker;
    myList.boughtPrice =list.price;
    myList.date = new Date()
    // wishList
    if (list.wishList) {
        db.user_profile.update({id:list.userId}, {$push:{wishList:myList}},function(err,list){
            if(err){
                res.send(err);
            }
            res.json(list);
        });
    }
    // ownlist
    if (list.ownList){
        db.user_profile.update({id:list.userId}, {$push:{ownList:myList}},function(err,list){
            if(err){
                res.send(err);
            }
            res.json(list);
        });
    }
});

router.put('/removeStocks', function(req, res, next){
    var list = req.body;
    var myList = {};
    if(!list.userId){
        res.status(400);
        res.json({
            "error":"Bad Data"
        });
    }
    var whatList = "";
    myList.ticker = list.ticker;
    myList.boughtPrice =list.price;
    myList.date = new Date()

    // wishList
    if (list.wishList) {
        db.user_profile.update({id:list.userId}, {$pull:{wishList:{ticker: list.ticker}}},function(err,list){
            if(err){
                res.send(err);
            }
            res.json(list);

        });
    }
    // ownlist
    if (list.ownList){
        db.user_profile.update({id:list.userId}, {$pull:{ownList:{ticker: list.ticker}}},function(err,list){
            if(err){
                res.send(err);
            }
            res.json(list);
        });
    }
})

router.post('/info', function(req, res, next){
    var reqBody = req.body;
    var usr = {}
    usr.userId = reqBody.userId;
    usr.dateCreated = new Date();
    usr.ownList =[
        {
            "ticker":"GRPN",
            "price" :3.50,
            "dateAdded":new Date()
        },
        {
            "ticker":"APPL",
            "price" :100.50,
            "dateAdded":new Date()
        }
    ]
    usr.WishList = [];

        db.user_profile.save(usr, function(err, task){
            if(err){
                res.send(err);
            }
            res.json(usr);
        });
});

module.exports = router;
