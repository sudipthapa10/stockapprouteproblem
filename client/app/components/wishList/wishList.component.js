"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var wishList_service_1 = require("../../services/wishList.service");
var WishListComponent = (function () {
    function WishListComponent(service) {
        this.service = service;
        this.wish = [];
        this.own = [];
        this.lists = [];
        this.submitted = false;
    }
    WishListComponent.prototype.onSubmit = function () { this.submitted = true; };
    WishListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.service.getWishList().subscribe(function (lists) { console.log(lists); _this.lists = lists; });
    };
    WishListComponent.prototype.addToWishList = function (event, list) {
        var _this = this;
        event.preventDefault();
        //hardcoded userName , this need to be changed with authentication
        var newWishList = {
            _id: "sudipthapa10",
            ind: "wish",
            ticker: list.ticker,
            price: list.price
        };
        var newObj = {
            ticker: list.ticker,
            price: list.price
        };
        this.service.saveStockList(newWishList).subscribe(function (res) {
            for (var i = 0; i < res.length; i++) {
                if (res[i].ticker == newObj["ticker"].toUpperCase() && res[i]["price"] == newObj["price"]) {
                    console.log(_this.lists.wishList);
                    _this.lists.wishList.push(res[i]);
                }
            }
        });
    };
    WishListComponent.prototype.addToOwnedList = function (event, list) {
        var _this = this;
        event.preventDefault();
        //hardcoded userName , this need to be changed with authentication
        var newWishList = {
            _id: "sudipthapa10",
            ind: "own",
            ticker: list.ownticker,
            price: list.ownprice
        };
        var newObj = {
            ownticker: list.ownticker,
            ownprice: list.ownprice
        };
        this.service.saveStockList(newWishList).subscribe(function (res) {
            for (var i = 0; i < res.length; i++) {
                console.log(res[i].ticker);
                if (res[i].ticker == newObj["ownticker"].toUpperCase() && res[i].price == newObj["ownprice"]) {
                    _this.lists.ownList.push(res[i]);
                }
            }
        });
    };
    WishListComponent.prototype.deleteWishList = function (event, _id) {
        var _this = this;
        console.log(_id);
        event.preventDefault();
        var obj = {
            childId: _id
        };
        this.service.deleteWishList(obj).subscribe(function (data) {
            var currentList = _this.lists.wishList;
            console.log(currentList);
            for (var i = 0; i < currentList.length; i++) {
                if (currentList[i]._id == _id) {
                    _this.lists.wishList.splice(i, 1);
                    break;
                }
            }
        });
    };
    WishListComponent.prototype.deleteOwnList = function (event, _id) {
        var _this = this;
        event.preventDefault();
        var obj = {
            childId: _id
        };
        this.service.deleteOwnList(obj).subscribe(function (data) {
            var currentList = _this.lists.ownList;
            console.log(currentList);
            for (var i = 0; i < currentList.length; i++) {
                if (currentList[i]._id == _id) {
                    _this.lists.ownList.splice(i, 1);
                    break;
                }
            }
        });
    };
    WishListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'wish-list-form',
            templateUrl: './wishList.component.html',
            providers: [wishList_service_1.WishListService]
        }), 
        __metadata('design:paramtypes', [wishList_service_1.WishListService])
    ], WishListComponent);
    return WishListComponent;
}());
exports.WishListComponent = WishListComponent;
//# sourceMappingURL=wishList.component.js.map