var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');

var db = mongojs('mongodb://prix:prix@ds019058.mlab.com:19058/stock_predict_goru', ['tasks', 'stocks', 'current_stock_data']);


/*

//api_key = n5whJFJt6J8yU5ba4W55
var http = require('https');
var url = "https://www.quandl.com/api/v3/datatables/WIKI/PRICES.json?date.gte=20170119&date.lt=20170120&api_key=n5whJFJt6J8yU5ba4W55";

http.get(url, function(res){
    var body = '';

    res.on('data', function(chunk){
        body += chunk;
    });

    res.on('end', function(){
        var fbResponse = JSON.parse(body);
        //console.log("Got a response: ", fbResponse.datatable);

        //PSINGH ENABLE THIS TO WRITE DATA EVERY TIME A PAGE IS REFRESHED... //PSINGH
        //processData(fbResponse.datatable);
    });
}).on('error', function(e){
    console.log("Got an error: ", e);
});
*/

router.get('/hotStock', function(req, res, next){
    db.current_stock_data.find(function(err, hotStock){
        if(err){
            res.send(err);
        }
        res.json(hotStock);
    });
});

//stock information
router.get('/stocks', function(req, res, next){
    db.stocks.find(function(err, tasks){
        if(err){
            res.send(err);
        }
        res.json(tasks);
    });
});


// Get All Tasks
router.get('/tasks', function(req, res, next){
    db.tasks.find(function(err, tasks){
        if(err){
            res.send(err);
        }
        res.json(tasks);
    });
});

//process data
function processData( fullStockData)
{
    var writeLineString ="";
    var percentChange = 0;
    var counter = 0 ;
    var openingPrice = 0;
    var currentPrice = 0;
    for(var attributename in fullStockData.data){
        openingPrice = fullStockData.data[attributename][2];
        currentPrice = fullStockData.data[attributename][5];
        percentChange  = ((currentPrice / openingPrice) * 100) - 100
        if( percentChange >= 5 || percentChange <= -5) {
            writeLineString = '{"ticker":' + '"' + fullStockData.data[attributename][0] + '"' +
                ',"date":' + '"' + fullStockData.data[attributename][1] + '"' +
                ', "openPrice":' + openingPrice +
                ', "closePrice":' + currentPrice +
                ', "percentChange":' + percentChange.toFixed(2) +
                ', "lastChange": "' +  new Date() + '"}';
            saveProcessData(JSON.parse(writeLineString));
            counter += 1;
        }
    }
    console.log("ALL DONE  Counter:  => " + counter);
}

function saveProcessData(saveData){
    db.current_stock_data.save(saveData, function(err, saveData){
        if(err)
            console.log("error happending during writing into database");
        //else
            //console.log('succesfully wrote into database');
    });
}

// Get Single Task
router.get('/task/:id', function(req, res, next){
    db.tasks.findOne({_id: mongojs.ObjectId(req.params.id)}, function(err, task){
        if(err){
            res.send(err);
        }
        res.json(task);
    });
});

//Save Task
router.post('/task', function(req, res, next){
    var task = req.body;
    if(!task.title || !(task.isDone + '')){
        res.status(400);
        res.json({
            "error": "Bad Data"
        });
    } else {
        db.tasks.save(task, function(err, task){
            if(err){
                res.send(err);
            }
            res.json(task);
        });
    }
});

// Delete Task
router.delete('/task/:id', function(req, res, next){
    db.tasks.remove({_id: mongojs.ObjectId(req.params.id)}, function(err, task){
        if(err){
            res.send(err);
        }
        res.json(task);
    });
});

// Update Task
router.put('/task/:id', function(req, res, next){
    var task = req.body;
    var updTask = {};
    
    if(task.isDone){
        updTask.isDone = task.isDone;
    }
    
    if(task.title){
        updTask.title = task.title;
    }
    
    if(!updTask){
        res.status(400);
        res.json({
            "error":"Bad Data"
        });
    } else {
        db.tasks.update({_id: mongojs.ObjectId(req.params.id)},updTask, {}, function(err, task){
        if(err){
            res.send(err);
        }
        res.json(task);
    });
    }
});

module.exports = router;

