
export class Stock{
    ticker: string;
    price: string;
    openPrice : number;
    closePrice : number;
    percentChange : number;
    lastChange: String;
}