import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
//import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Stock } from '../components/stock/Stock';


@Injectable()
export class StockService {
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http:Http){
        console.log('Stock Service Initialized...');
    }
    private stock:Stock[] = [];
    getStocks(){
      var response =  this.http.get('/api/stocks')
            .map(response => response.json());
        return response;
    }

    saveStockList(lists){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/api/saveList', JSON.stringify(lists), {headers: headers})
            .map(res => res.json());
    }

}