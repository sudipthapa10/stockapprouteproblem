var usr = require('../models/userStocks');

module.exports = {
    getStocksLists: function (req, res) {
        usr.findOne({_id: "sudipthapa10"}, function (err, col) {
            if (err) {
                console.log("errr", err);
                //return done(err, null);
            } else {
                res.send(col);
            }
        });
    },
    saveWishList: function (req, res, next) {
        var data = req.body;
        var stock = new usr();
        if (data.ind == "wish") {
            var list = {
                ticker: data.ticker.toUpperCase(),
                price: data.price,
                dateAdded: new Date()
            };
            usr.findOne({"_id": data._id}, function (err, parent) {
                    if (err) {
                        res.send("can't save !!")
                    }
                    else {
                        //check if wishlist already exists
                        var found = false;
                        for (var i = 0; i < parent.wishList.length; i++) {
                            if (parent.wishList[i].ticker == data.ticker.toUpperCase()) {
                                found = true;
                            }
                        }
                        if (!found) {
                            parent.wishList.push(list);
                            parent.save(function (err, data) {
                                if (err) return handleError(err);
                                res.json(data.wishList);
                            });
                        } else {
                            res.json([]);
                        }
                    }
                }
            );
        }
        else if (data.ind == "own") {
            var list = {
                ticker: data.ticker.toUpperCase(),
                price: data.price,
                dateAdded: new Date()
            };
            usr.findOne({"_id": data._id}, function (err, parent) {
                    if (err) {
                        res.send("can't save !!")
                    }
                    else {
                        var found = false;
                        for (var i = 0; i < parent.ownList.length; i++) {
                            if (parent.ownList[i].ticker == data.ticker.toUpperCase()) {
                                found = true;
                            }
                        }
                        if (!found) {
                            parent.ownList.push(list);
                            parent.save(function (err, data) {
                                if (err) return handleError(err);
                                res.json(data.ownList);
                            });
                        } else {
                            res.json([]);
                        }
                    }
                }
            );
        } else {
            res.send("Please add to wish List or owned list");
        }
    },
    deleteWishList: function (req, res, next) {
        var data = req.body;
        var childId = data.childId;
        usr.findOne({_id: "sudipthapa10"}, function (err, parent) {
            if (err) {
                console.log("You do not have permission LOL! ", err);
                //return done(err, null);
            } else {
                parent.wishList.id(childId).remove();
                parent.save(function (err) {
                    if (err) return handleError(err);
                    console.log('the sub-doc was removed');
                    res.json({_id: childId});
                });
            }
        });
    },
    deleteOwnList: function (req, res, next) {
        var data = req.body;
        var childId = data.childId;
        usr.findOne({_id: "sudipthapa10"}, function (err, parent) {
            if (err) {
                console.log("You do not have permission LOL! ", err);
                //return done(err, null);
            } else {
                parent.ownList.id(childId).remove();
                parent.save(function (err) {
                    if (err) return handleError(err);
                    console.log('the sub-doc was removed');
                    res.json({_id: childId});
                });
            }
        });
    }
}
