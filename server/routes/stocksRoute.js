var express = require('express');
var stockController = require ('../controller/stockController');
var usrController = require ('../controller/userProfileController');

var router= express.Router();
router.get('/stocks',stockController.getStocks);
router.post('/stocks',stockController.saveFromRouter);
//router.get('/stockList',usrController.getStocksLists);


module.exports = router;